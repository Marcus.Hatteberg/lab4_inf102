package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int n = list.size();
        while(n>0){
            int last_mod_index = 0;
            for(int cur_index = 1; cur_index < n; cur_index++){
                if(list.get(cur_index-1).compareTo(list.get(cur_index)) > 0){
                    T tmp = list.get(cur_index-1);
                    list.set(cur_index-1, list.get(cur_index));
                    list.set(cur_index, tmp);
                    last_mod_index = cur_index;
                }
            }
            n = last_mod_index;
        }
    }
    
}

package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;


public class QuickMedian implements IMedian {
    static <T extends Comparable<T>> int partition(List<T> list, int lo, int hi) 
    { 
        T pivot = list.get(hi);
        int z = (lo-1);
        for (int i=lo; i<hi; i++)
        {
            if (list.get(i).compareTo(pivot) < 0)
            {
                z++;
                T tmp = list.get(z);
                list.set(z, list.get(i));
                list.set(i, tmp);
            } 
        } 
        T tmp = list.get(z+1);
        list.set(z+1, list.get(hi));
        list.set(hi, tmp);
        
        return z+1; 
    }

    public static <T extends Comparable<T>> T kselection(List<T> list, int lo, int hi, int k) 
    { 
        int partition_sorting_value = partition(list,lo,hi);

        if(partition_sorting_value == k){ return list.get(partition_sorting_value); }
        else if(partition_sorting_value < k ){ return kselection(list, partition_sorting_value + 1, hi, k ); }
        else{ return kselection(list, lo, partition_sorting_value-1, k ); }
    }

    //Didnt want to remove good ol' slowboy :D but commented out though

    //public static <T extends Comparable<T>> void quicksort(List<T> list, int from, int to) {
    //    if (from < to) {
    //        int pivot = from;
    //        int left = from + 1;
    //        int right = to;
    //        T pivotValue = list.get(pivot);
    //        while (left <= right) {
    //            while (left <= to && list.get(left).compareTo(pivotValue) >= 0) {
    //                left++;
    //            }
    //            while (right > from && list.get(right).compareTo(pivotValue) < 0) {
    //                right--;
    //            }
    //            if (left < right) {
    //                Collections.swap(list, left, right);
    //            }
    //        }
    //        Collections.swap(list, pivot, left - 1);
    //        quicksort(list, from, right - 1);
    //        quicksort(list, right + 1, to);
    //    }
    //}

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> list_copy = new ArrayList<>(list); // Method should not alter list
        
        int list_size = list_copy.size();
        return kselection(list_copy,0,list_size-1,list_size/2);
    }

}
